# Challenge 2

This challenge describes how to run your first container!

----

### Go to flask app folder

##### command:

```bash
cd flask-app
```
----

### In the flask-app folder build the image:

##### command:

```bash
docker build -t ssti .
```

##### output:

```bash
Sending build context to Docker daemon  1.283MB
Step 1/7 : FROM alpine:3.7
3.7: Pulling from library/alpine
5d20c808ce19: Pull complete 
Digest: sha256:8421d9a84432575381bfabd248f1eb56f3aa21d9d7cd2511583c68c9b7511d10
Status: Downloaded newer image for alpine:3.7
 ---> 6d1ef012b567
Step 2/7 : MAINTAINER Glenn ten Cate <glenn.ten.cate@owasp.org>
 ---> Running in 462adab80be2
Removing intermediate container 462adab80be2
 ---> 7a52d33b0c9d
Step 3/7 : RUN apk update --no-cache && apk add git python2-dev py2-pip git bash
 ---> Running in 575b13fb901b
 ........ snip .......
```
----

### Run the container

##### command:

``` bash
docker run -p5000:5000 ssti
```

#### output:

```bash
* Serving Flask app "SSTI" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 173-090-991
```

----

### Find the running state:

##### command:

```bash
docker ps
```

##### output:

```bash
CONTAINER ID        IMAGE               COMMAND               CREATED             STATUS              PORTS                    NAMES
42a644cea96b        ssti                "python2 ./SSTI.py"   2 minutes ago       Up 2 minutes        0.0.0.0:5000->5000/tcp   gracious_nobel
```
----

### Hans, get ze flammenwerfer

##### command:

```bash
docker kill 42a644cea96b
```

##### output:

```bash
42a644cea96b
```