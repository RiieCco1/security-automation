# Challenge 11 - .Git in Docker

##### Go the the app folder:

```bash
cd app
```

#### build the container:

```bash
docker build -t gitchallenge .
```
----

### Run the gitchallenge container

##### command:

```bash 
docker run -p80:80 gitchallenge
```

#### Hack it

can you find the secret from the app?

----
